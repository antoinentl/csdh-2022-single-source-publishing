---
title: "The Importance of Single Source Publishing in Scientific Publishing"
resume: "Poster created for the DH Unbound conference"
color: "ffead0-3786b3-265d8a-f26419-000000"
source: "ssp.digitaltextualities.ca"
repository: "https://gitlab.huma-num.fr/ecrinum/communications/csdh-2022-single-source-publishing"
credits: "Antoine Fauchié, Margot Mellet, Marcello Vitali-Rosati. Canada Research Chair on Digital Textualites (CC-BY)."
bibliography: "bibliography.bib"
lang: en
csl: "apa.csl"
nocite: |
  @*
foot: "This poster was made with Vim, Markdown, YAML, Pandoc and Git. It exists in different versions at this address: ssp.digitaltextualities.ca"
formats:
  - name: web
    address: index.html
  - name: poster
    address: poster.html
  - name: XML-TEI
    address: poster.tei
  - name: txt
    address: plain.txt
  - name: repository
    address: "https://gitlab.huma-num.fr/ecrinum/communications/csdh-2022-single-source-publishing"
contact: "Antoine Fauchié, antoine.fauchie@umontreal.ca"
---

::: {.un}
## What is Single Source Publishing?
**The expression _Single Source Publishing_ refers to generating several formats from a single source.**
One and unique source can be used to produce various artifacts, without having to switch from one working progress to another.
With a Single Source Publishing approach it's possible to produce a PDF format for printing, an XML export for a digital platform or a digital version in HTML format, just with _one_ source.
What is this "source"?
A set of texts, some metadata, some bibliographical data, perhaps some images and other medias.
Perhaps you already use the principles of Single Source Publishing: it's long and tedious with a classic word processor, it's a little bit complex with LaTeX, it's expensive and it's not easy with XML, and it's very powerful and more accessible with tools like Pandoc.

**This editorial challenge brings up both theoretical and technical questions, such as the legitimization of content, the evolution of publishing practices, and the creation of adequate tools.**
:::

::: {.deux}
![](images/single-source-publishing.png)
:::

::: {.trois}
## Academic publishing constraints
Scientific publishing has several constraints:

- **critical material**: footnotes, citations, bibliographies, figures, insert, etc.
- **metadata**: data about data, like title, subtitle, authors, standard identifiers (ORCID, Wikidata, etc.)
- **text structuration**: semantic is a necessity for digital diffusion (especially in XML)
- **bibliographic data**: structured references
- **peer review**: a complex circulation and validation for the texts
- **specific formats**: articles, books, conference proceedings, etc.
- **publish or perish**: publish a lot but slowly
:::

::: {.quatre}
## Different formats: in and out
**In**: with non-standard formats like `.doc`/`.docx`, it's not possible to produce richly structured content. XML is a good solution but it's complex and there are not enough customizable tools to write and edit. Lightweight and understandable markup languages is an interesting intermediate solution, like Markdown or AsciiDoc.

**Out**: scientific publishing requires specific formats: PDF, HTML, and various XML with specific schemas (like JATS or TEI).
:::

::: {.cinq}
## Benefits of Single Source Publishing

- **one source**: no need to manage multiple versions, like the `.doc` version, the `.xml` version and the `.indd` version
- **horizontal workflow**: all actors of the publishing chain can participate at the same time (in theory). Writers, publishers, designers and digital distributors can work at the same time
- **multimodal**: one source but multiple artifacts/formats
- **time and energy saving** 
- **work in progress**: building a publishing chain can be a work in progress
:::

::: {.six}
## Issues involved in the Single Source Publishing principle 
### Legitimization of content
If the publishing chain can be horizontal, how can we do the legitimization of the content?
How to deal with the validation of content, the structuration and the design of the document?

### Evolution of publishing practices

- no more word processing (or much more less)
- more tech in publishing staff (but less complexity)
- publishers build their own publishing chain by assembling free programs/software
- publishing workflows can be fun!

### Creation of adequate tools
Since the 1980's, scientific community use one and unique tool for writing and editing: different generations of word processors like Microsoft Word, LibreOffice Writer or Google Docs.
Proprietary, copycat of proprietary software or centralised, they maintain a confusion between the structure of the contents and the graphic rendering.

**The academic community needs tools that correspond to its constraints!**
And it's what a lot of people do, with an approach more or less compatible with the Single Source Publishing: Manifold, PubPubPub, Coko, Métopes, Quire, Stylo, etc.
:::

::: {.sept}
## Concepts
With Single Source Publishing come some interesting concepts:

### Hybridity
From Marshall McLuhan [@mcluhan_understanding_1965]: the hybridity of the media generates new media: the media have an effect on each other. In the same way, it is interesting to take into account the influence of various formats on their unique source.

### Hybridization
From Alessandro Ludovico [@ludovico_post-digital_2012]: the hybridization of forms and formats is a phenomenon that can be observed in the first digital experiments: electronic versions come to complete already existing forms, printed and digital artifacts become hybrid.

### Editorialization
From Marcello Vitali-Rosati [@vitali-rosati_what_2016]: editorialization is the set of dynamics that produce and structure digital space. The adaptation of publishing chains to produce different artifacts is one way of understanding and constructing this space.
:::

::: {.huit}
## Academic Single Source Publishing in practice: an example
In the **Revue2.0 project** ([revue20.org](https://revue20.org)) lead by the Canada Research Chair on Digital Textualities, several journals have experimented Single Source Publishing with Stylo (a semantic editor for academic writing and publishing). With one source (a set of text, metadata, bibliographic data), scientific journals are able to produce the following formats: HTML for their website, PDF for their own distribution and for aggregators, and XML formats for digital distributors.

**Stylo** is a tool designed to transform the digital workflow of scholarly journals in humanities and social sciences. As a WYSIWYM (What You See Is What You Mean) semantic text editor for the humanities, it aims to improve the academic publishing chain.
Stylo is ready and free to use: [stylo.huma-num.fr](https://stylo.huma-num.fr).
:::

::: {.neuf}
## References

::: {#refs}
:::

:::
